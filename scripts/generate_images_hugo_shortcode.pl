#! /usr/bin/perl
use strict;
use warnings;
use Image::Size;
use Image::ExifTool ;

my ($dir, $number) = @ARGV;
# shortcode for my hugo template tranquilpeak
my $imageShortcode ="{{< image classes=\"clear center fancybox\" src=\"%s\" thumbnail-width=\"%s%%\"  title=\"%s\"  >}} \n";

my @files = glob( $dir . '/*' );

foreach my $file (@files)
{

	# Get Exif information from image file
	my $exifTool = new Image::ExifTool;

	my $info = $exifTool->ImageInfo($file);
	my $dateTaken = $$info{'DateTimeOriginal'};
	my $title='';
	# extract path starting from /img/ folder
	my $fileName= substr($file,index($file,'/img/'));
	if ($dateTaken) {
		$title=$dateTaken . ' (' . $$info{'LensID'}.')';

	}

	my ($width, $height) = imgsize($file);	
	# portrait mode
	my $thumbnailWidth=80;
	if($width <= $height){
		$thumbnailWidth=50;
	}
	printf($imageShortcode, $fileName,$thumbnailWidth,$title);
}

